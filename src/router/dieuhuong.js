import React, { PureComponent } from 'react'
import User from '../component/user'
import Task from '../component/task'
import Newtask from '../component/newtask'
import DetailWithID from '../component/detailTask/detailWithID'
import { Route } from "react-router-dom"

export default class dieuhuong extends PureComponent {
    render() {
        return (
            <div>
                <Route exact path="/" render={() => <User />} />
                <Route path="/task" render={() => <Task />} />
                <Route path="/newtask" render={() => <Newtask />} />
                <Route path="/findtask/:id" render={(props) => <DetailWithID {...props} />} />

            </div>
        )
    }
}