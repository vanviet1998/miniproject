import { userReducer1 } from '../reducer/userReducer'
import { taskReducer1 } from '../reducer/taskReducer'
import { createStore,combineReducers } from 'redux';

const rootReducer = combineReducers({
     user: userReducer1,
     taskReducer: taskReducer1
});

let store = createStore(rootReducer)
export default store