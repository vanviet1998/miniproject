import { Get_TASK, EDIT_TASK, FIND_TASK } from '../typeAction'


var LocalTask =  JSON.parse(localStorage.getItem("Task1") || "[]");
const initialState = {
    task:LocalTask,
    taskfind: {}
};

export function taskReducer1(state = initialState, action) {
    switch (action.type) {
        case Get_TASK:
         var Task = JSON.parse(localStorage.getItem("Task1") || "[]");
            Task.push(action.task)
            console.log(Task)
            localStorage.setItem("Task1", JSON.stringify(Task));
         return {
            ...state,
            task: Task

        }
        case FIND_TASK:
            console.log(state.task)
            let findTask = {}
            findTask = [...state.task].filter(item => item.id === action.id)
            console.log(findTask)
            return { ...state, taskfind: findTask[0] }
        case EDIT_TASK:

            let newTask = [...state.task].filter(item => item.id !== action.task.id)
            newTask = [...newTask, action.task]
            console.log(newTask)
            localStorage.setItem("Task1", JSON.stringify(newTask));
            return {
                ...state,
                task: newTask
            }
        default:
            return state
    }
}