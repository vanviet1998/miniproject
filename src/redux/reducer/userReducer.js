import { Get_USER } from '../typeAction'

const initialState = {
    user: {
        name: '',
        date: '',
        age: '',
        sex: ''
    }
};
export function userReducer1(state = initialState, action) {
    switch (action.type) {
        case Get_USER:
            return { ...state, user: action.User }
        default:
            return state
    }
}
