import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import { getUser } from '../redux/action/userAction'
import { Link } from "react-router-dom";
import PropTypes from 'prop-types';

class user extends PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            date: '',
            age: '',
            sex: ''
        }
    }
    getDetailUser = (event) => {
        let name = event.target.name
        let val = event.target.value
        this.setState({
            [name]: val
        })
    }
    onClickgetUser = () => {
        let user = {
            name: this.state.name,
            date: this.state.date,
            age: this.state.age,
            sex: this.state.sex
        }
        this.props.getUser(user)
    }
    render() {

        return (
            <form action="/" style={{ maxWidth: '400px', margin: 'auto' }}>
                <div className="form-group">
                    <label >Name</label>
                    <input type="text" name='name' onChange={(event) => this.getDetailUser(event)} className="form-control" aria-describedby="helpId" />
                </div>
                <div className="form-group">
                    <label htmlFor="exampleInputPassword1">Date of Birth</label>
                    <input type="date" name='date' onChange={(event) => this.getDetailUser(event)} className="form-control" id="exampleInputPassword1" />
                </div>
                <div className="form-group">
                    <label htmlFor="exampleInputPassword1">Age</label>
                    <input type="number" name='age' onChange={(event) => this.getDetailUser(event)} className="form-control" id="exampleInputPassword1" />
                </div>
                <div className="form-group">
                    <select name='sex' className="form-control" onChange={(event) => this.getDetailUser(event)} >
                        <option value="true">Nam</option>
                        <option value="false">Nu</option>
                    </select>
                </div>
                <Link to="/task" onClick={() => this.onClickgetUser(user)} className="btn btn-outline-primary">Submit</Link >
                <button type="reset" className="btn btn-outline-primary">Reset</button>
            </form>
        )
    }
}
const mapStateToProps = (state, ownProps) => {
    return {
        user: state.user
    }
}
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        getUser: (user) => {
            dispatch(getUser(user))
        }
    }
}
user.propTypes = {
    user: PropTypes.object,
    getTask: PropTypes.func,
}
export default connect(mapStateToProps, mapDispatchToProps)(user)