import React, { PureComponent } from 'react'
import { Link } from "react-router-dom";
import PropTypes from 'prop-types';
import { editTask, findTask } from '../../redux/action/taskAction'
import { connect } from 'react-redux'

class componentName extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            id: '', nameTask: '', detailTask: '', typeTask: ''
        }
    }
    clickEdit = async () => {
        await this.setState({
            id: this.props.id,
            nameTask: this.props.name,
            detailTask: this.props.detail,
            typeTask: this.props.typeTask
        })
        this.props.getEditTask(this.state)
    }
    clickEdit1 = async () => {
        await this.setState({
            id: this.props.id,
            nameTask: this.props.name,
            detailTask: this.props.detail,
            typeTask: this.props.typeTask
        })
        this.props.findTask(this.state)
    }
    render() {
        return (
            <ul className="list-group list-group-flush">
                <li className="list-group-item "> {this.props.name}
                    <div className="congcu float-right ml-4">
                        <Link to={'/findtask/' + this.props.id}  className="btn btn-warning">Edit</Link>
                        <button onClick={() => this.clickEdit()} type="button" data-toggle="modal" data-target="#myModal1" className="btn btn-danger">Edit</button>
                    </div>
                </li>
            </ul>
        )
    }

}
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        editTask: (task) => {
            dispatch(editTask(task))
        },
        findTask: (task) => {
            dispatch(findTask(task))
        }
    }
}
export default connect(null, mapDispatchToProps)(componentName)