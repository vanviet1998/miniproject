import React, { Component } from 'react'
import { connect } from 'react-redux'
import { findTask, editTask } from '../../redux/action/taskAction'
import PropTypes from 'prop-types';
import { Link } from "react-router-dom";

class detailWithID extends Component {
    constructor(props) {
        super(props)
        this.state = {
            id: '',
            nameTask: '',
            detailTask: '',
            typeTask: ''
        }
    }
    clickEdit = () => {
        let task = {
            id: this.props.match.params.id.toString(),
            nameTask: this.state.nameTask,
            detailTask: this.state.detailTask,
            typeTask: this.state.typeTask
        }
        console.log(task)
        this.props.editTask(task)
    }
    getDetailTask = async (event) => {

        let name = event.target.name
        let val = event.target.value
        await this.setState({
            [name]: val,
        })
    }
    componentWillMount() {
        console.log('will')
        this.props.findTask(this.props.match.params.id.toString())
    }
  async  componentWillReceiveProps(nextProps) {
        if (nextProps.task !== undefined) {
            console.log(nextProps.task.nameTask)
            await  this.setState({
                id: this.props.match.params.id.toString(),
                nameTask: nextProps.task.nameTask,
                detailTask: nextProps.task.detailTask,
                typeTask: nextProps.task.typeTask
            })

        }
    }


    render() {

        console.log(this.props.task)
        return (
            <form action="/" style={{ maxWidth: '400px', margin: 'auto' }}>
                <div className="form-group">
                    <label >Name task</label>
                    <input type="text" name='nameTask' onChange={(event) => this.getDetailTask(event)} defaultValue={this.state.nameTask} className="form-control" aria-describedby="helpId" />
                </div>
                <div className="form-group">
                    <label htmlFor="exampleInputPassword1">Detail</label>
                  <textarea name='detailTask' onChange={(event) => this.getDetailTask(event)} className="form-control" value={this.state.detailTask}/> 
                </div>
                <div className="form-group">
                    {/* <label htmlFor="exampleInputPassword1">creator:{this.props.userTask.name}</label> */}
                </div>
                <div className="form-group">
                    <select name='typeTask' onChange={(event) => this.getDetailTask(event)} className="form-control" value={this.props.task.typeTask}>
                        <option value="new" >New</option>
                        <option value="proces">In Process</option>
                        <option value="resoled">resoled</option>
                    </select>

                </div>
                <Link to='/task' onClick={() => this.clickEdit()} className="btn btn-outline-primary">Submit</Link>
            </form>
        )
    }
}
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        findTask: (task) => {
            dispatch(findTask(task))
        },
        editTask: (task) => {
            dispatch(editTask(task))
        }
    }
}
const mapStateToProps = (state, ownProps) => {
    return {
        task: state.taskReducer.taskfind
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(detailWithID)