import React, { PureComponent } from 'react'
import PropTypes from 'prop-types';
import { Link } from "react-router-dom";
import { connect } from 'react-redux'
import { getTask } from '../redux/action/taskAction'

class newtask extends PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            id: '',
            name: '',
            detail: '',
            typeTask: ''
        }
    }
    getDetailTask = (event) => {
        var name = event.target.name
        var val = event.target.value
        this.setState({
            [name]: val
        })

    }
    clickGetTask = () => {
        var Task = {
            id: this.state.id,
            nameTask: this.state.name,
            detailTask: this.state.detail,
            typeTask: 'new'
        }
        this.props.getTask(Task)

       
    }
    render() {

        return (
            <form action="/" style={{ maxWidth: '400px', margin: 'auto' }}>
                <div className="form-group">
                    <label >Id Task</label>
                    <input type="text" name='id' onChange={(event) => this.getDetailTask(event)} className="form-control" aria-describedby="helpId" />
                </div>
                <div className="form-group">
                    <label >Name task</label>
                    <input type="text" name='name' onChange={(event) => this.getDetailTask(event)} className="form-control" aria-describedby="helpId" />
                </div>
                <div className="form-group">
                    <label htmlFor="exampleInputPassword1">Detail</label>
                    <textarea className="form-control" name='detail' onChange={(event) => this.getDetailTask(event)} />
                </div>
                <div className="form-group">
                    <label htmlFor="exampleInputPassword1">creator:{this.props.user.name}</label>
                </div>
                <Link to="/task" onClick={() => this.clickGetTask()} className="btn btn-outline-primary">Submit</Link>
            </form>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        user: state.user.user
    }
}
newtask.propTypes = {
    user: PropTypes.object,
    getTask: PropTypes.func
}
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        getTask: (task) => {
            dispatch(getTask(task))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(newtask)
